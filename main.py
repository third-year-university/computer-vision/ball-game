import random

import cv2
import numpy as np

blue = [98, 232, 120]
blue_lower = np.array([blue[0] - 15, blue[1] - 30, 0])
blue_higher = np.array([blue[0] + 15, blue[1] + 30, 255])

orange = [7, 230, 236]
orange_lower = np.array([orange[0] - 15, orange[1] - 30, 0])
orange_higher = np.array([orange[0] + 15, orange[1] + 30, 255])

green = [58, 135, 127]
green_lower = np.array([green[0] - 15, green[1] - 30, 0])
green_higher = np.array([green[0] + 15, green[1] + 30, 255])

cam = cv2.VideoCapture(0)
cam.set(cv2.CAP_PROP_AUTO_EXPOSURE, 1)
cam.set(cv2.CAP_PROP_EXPOSURE, 500)


def initialise():
    global x_mark
    global y_mark
    global last
    global to_find
    global text_color
    global text_to_show
    global balls_coords
    text_color = (0, 0, 0)
    text_to_show = ""
    x_mark = 40
    y_mark = 40
    to_find = []
    balls_coords = []
    colors_avalible = ["green", "orange", "blue"]
    while len(to_find) < 3:
        new_color = colors_avalible[random.randint(0, 2)]
        if new_color not in to_find:
            to_find.append(new_color)


initialise()
# cv2.namedWindow("Camera", cv2.WINDOW_KEEPRATIO)

while cam.isOpened():
    if len(balls_coords) < 3:
        balls_coords = []
    ret, frame = cam.read()
    frame = cv2.flip(frame, 1)
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    mask_blue = cv2.inRange(hsv, blue_lower, blue_higher)
    mask_blue = cv2.erode(mask_blue, None, iterations=2)
    mask_blue = cv2.dilate(mask_blue, None, iterations=2)
    contours_blue, _ = cv2.findContours(mask_blue, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours_blue) > 0:
        c = max(contours_blue, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 50:
            cv2.circle(frame, (int(x), int(y)), int(radius), (255, 0, 0), 2)
            if len(balls_coords) < 3:
                balls_coords.append((x, y, "blue"))

    cv2.imshow("Mask_blue", mask_blue)

    mask_green = cv2.inRange(hsv, green_lower, green_higher)
    mask_green = cv2.erode(mask_green, None, iterations=2)
    mask_green = cv2.dilate(mask_green, None, iterations=2)
    contours_green, _ = cv2.findContours(mask_green, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours_green) > 0:
        c = max(contours_green, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 50:
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 214, 120), 2)
            if len(balls_coords) < 3:
                balls_coords.append((x, y, "green"))
    cv2.imshow("Mask_green", mask_green)

    mask_orange = cv2.inRange(hsv, orange_lower, orange_higher)
    mask_orange = cv2.erode(mask_orange, None, iterations=2)
    mask_orange = cv2.dilate(mask_orange, None, iterations=2)
    contours_orange, _ = cv2.findContours(mask_orange, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours_orange) > 0:
        c = max(contours_orange, key=cv2.contourArea)
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 50:
            if len(balls_coords) < 3:
                balls_coords.append((x, y, "orange"))
            cv2.circle(frame, (int(x), int(y)), int(radius), (0, 102, 255), 2)

    cv2.imshow("Mask_orange", mask_orange)


    if len(balls_coords) == 3:
        balls_coords.sort()


        xi = x_mark

        for mark in balls_coords:
            color = ()
            match mark[2]:
                case 'green':
                    color = (0, 214, 120)
                case 'blue':
                    color = (255, 0, 0)
                case 'orange':
                    color = (0, 102, 255)
            cv2.circle(frame, (xi, y_mark), 20, color, -1)
            xi += 50

        xi = x_mark

        for mark in to_find:
            color = ()
            match mark:
                case 'green':
                    color = (0, 214, 120)
                case 'blue':
                    color = (255, 0, 0)
                case 'orange':
                    color = (0, 102, 255)
            cv2.circle(frame, (xi, y_mark + 50), 20, color, -1)
            xi += 50

        all_correct = True
        for i in range(len(to_find)):
            if balls_coords[i][2] != to_find[i]:
                all_correct = False
        if all_correct:
            text_to_show = "GREAT!"
            text_color = (0, 255, 0)
        else:
            text_to_show = "WRONG!"
            text_color = (0, 0, 255)

        cv2.putText(frame, text_to_show, (x_mark, y_mark + 150), cv2.FONT_HERSHEY_SIMPLEX, 1, text_color)

    cv2.imshow("Camera", frame)

    key = cv2.waitKey(1)

    if key == ord('1'):
        initialise()
